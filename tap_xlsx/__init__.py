#!/usr/bin/env python3

from __future__ import absolute_import
from .catalog import process_file_schemas
from .utils import *

import os
import json
import singer
import pandas as pd
import sys
import argparse
import copy

# TODO: Switch to dask
# https://stackoverflow.com/a/38208600

REQUIRED_CONFIG_KEYS = ["files"]
CONFIG = {}
STATE = {}

CATALOG = {
    'streams': []
}

logger = singer.get_logger()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help='Config file', required=False)
    parser.add_argument("-p", "--properties", help="Property selections", required=False)
    parser.add_argument("--catalog", help="Catalog file", required=False)
    parser.add_argument('-s', "--state", help="State file", required=False)
    parser.add_argument("-d", "--discover", action='store_true', help="Do schema discovery", required=False)
    return parser.parse_args()

def check_config(config, required_keys):
    missing_keys = [key for key in required_keys if key not in config]
    if missing_keys:
        logger.error("tap-xlsx: Config is missing required keys: {}".format(missing_keys))
        exit(1)

def load_config(args):
    if not args.config:
        logger.error("tap-xlsx: the following arguments are required: -c/--config")
        exit(1)

    config = load_json(args.config)

    xlsx_files_definition = config.get("xlsx_files_definition", None)
    if xlsx_files_definition:
        if os.path.isfile(xlsx_files_definition):
            config = load_json(xlsx_files_definition)
            check_config(config, REQUIRED_CONFIG_KEYS)
        else:
            logger.error("tap-xlsx: '{}' file not found".format(xlsx_files_definition))
            exit(1)
    else:
        check_config(config, REQUIRED_CONFIG_KEYS)

    CONFIG.update(config)

def do_sync():
    logger.info("Starting sync")

    for streamInfo in CATALOG["streams"]:
        process_stream(streamInfo)

    logger.info("Sync completed")

def process_stream(streamInfo):
    stream_id = streamInfo["tap_stream_id"]
    streamSource = json.loads(stream_id)

    filePath = streamSource["path"]
    sheet = streamSource["sheet"]
    stream_name = streamInfo["stream"]

    schema = streamInfo["schema"]

    logger.info("Syncing entity '" + stream_name + "' from file: '" + filePath + "'")
    singer.write_schema(stream_name, schema, streamInfo['key_properties'], bookmark_properties=None, stream_alias=None)

    start_row = 0
    if stream_id in STATE:
        start_row = STATE[stream_id]["current_row"] or 0
        logger.info('Found state for file: \'%s\'. Resuming from row %d', filePath, start_row)
    else:
        logger.info('No state was found for file: \'%s\'. Starting from the beginning', filePath)
        STATE[stream_id] = {'current_row': 0}

    singer.write_state(STATE)

    col_count = len(schema["properties"].keys())
    cols_to_read = range(0, col_count)

    logger.info("Table has %d columns", col_count)
    logger.info(schema["properties"].keys())

    limit = None
    if 'limit_per_file' in CONFIG:
        limit = CONFIG['limit_per_file']

    last_rows_count = 1
    while last_rows_count > 0:
        batch_size = 1000

        if limit != None:
            if start_row + 1 > limit:
                logger.warn('Stopping due to limit being reached: %d', limit)
                return
            if limit < start_row + batch_size:
                batch_size = limit - start_row
                logger.warn('Capping next batch due to limit almost being reached: %d', limit)

        df = pd.read_excel(filePath, header=None, sheet_name=sheet, skiprows=start_row + 1, nrows=batch_size, usecols=cols_to_read)

        logger.info("Found a table with shape '" + str(df.shape) + "'")

        logger.info("Loading data")
        for _, row in df.iterrows():
            try:
                record = {}
                for colIndex, col in enumerate(schema["properties"].keys()):
                    # conInfo = schema["properties"][col]
                    # TODO: change type based on colInfo["type"]
                    if colIndex in row:
                        record[col] = str(row[colIndex])

                singer.write_record(stream_name, record)
            except Exception as inst:
                logger.error("Failed to process row %s", row)
                logger.error(inst)

        last_rows_count = df.shape[0]

        STATE[stream_id]["current_row"] += last_rows_count
        singer.write_state(STATE)

        start_row += last_rows_count

def main():
    args = parse_args()

    load_config(args)

    if args.state:
        STATE.update(load_json(args.state))

    if args.catalog:
        CATALOG.update(load_json(args.catalog))
    else:
        CATALOG.update({'streams': process_file_schemas(CONFIG["files"])})

    if args.discover:
        print(json.dumps(CATALOG, indent=2))
        exit(0)

    do_sync()

if __name__ == '__main__':
    main()
