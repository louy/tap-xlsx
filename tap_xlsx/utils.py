#!/usr/bin/env python3

import json

def load_json(path):
    with open(path) as f:
        return json.load(f)
