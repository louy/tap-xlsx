#!/usr/bin/env python3
import os
import json
import pandas as pd
import sys
import copy
import singer

logger = singer.get_logger()

def process_file_schemas(configFiles):
  schemas = []
  for fileInfo in configFiles:
    schemas.extend(process_file_schema(fileInfo))
  return schemas

"""
Process a given file config and return a dictionary of schemas for all
files under that dictionary object
"""
def process_file_schema(fileInfo):
    schemas = []
    # determines if file in question is a file or directory and processes accordingly
    filePath = os.path.expanduser(fileInfo["file"])
    if not os.path.exists(filePath):
        logger.warning(filePath + " does not exist, skipping")
        return
    if os.path.isdir(filePath):
        filePath = os.path.normpath(filePath) + os.sep #ensures directories end with trailing slash
        logger.info("Discovering all XLSX files in directory '" + filePath + "' recursively")
        for filename in os.listdir(filePath):
            subInfo = copy.deepcopy(fileInfo)
            subInfo["file"] = filePath + filename
            schemas.extend(process_file_schemas(subInfo)) # recursive call
    else:
        file_schema = get_file_schema(fileInfo)
        if file_schema:
            schemas.append(file_schema)
    return schemas

def get_file_schema(fileInfo):
    filePath = os.path.expanduser(fileInfo["file"])
    if filePath[-4:] != ".xls" and filePath[-5:] != ".xlsx":
        logger.warning("Skipping non-xlsx file '" + filePath + "'")
        logger.warning("Please provide a XLSX file that ends with '.xlsx'; e.g. 'users.xlsx'")
        return

    logger.info("Discovering entity '" + fileInfo["entity"] + "' from file: '" + filePath + "' and sheet: '" + fileInfo["sheet"] + "'")
    df = pd.read_excel(filePath, header=[0], sheet_name=fileInfo["sheet"], nrows=1)

    logger.info("Found a table with shape '" + str(df.shape) + "'")

    columns = df.columns
    schema = get_schema_from_columns(columns)

    stream_name = fileInfo["entity"]
    tap_stream_id = json.dumps({'path': filePath, 'sheet': fileInfo["sheet"]})

    key_properties = fileInfo["keys"]

    return {
                'stream': filePath,
                'tap_stream_id': tap_stream_id,
                'schema': schema,
                'key_properties': key_properties,
                'metadata' : singer.metadata.get_standard_metadata(schema,
                                    stream_name,
                                    key_properties)
            }

def get_schema_from_columns(columns):
    schema =    {
                    "type": "object",
                    "properties": {}
                }
    for column in columns:
        #for now everything is a string; ideas for later:
        #1. intelligently detect data types based on a sampling of entries from the raw data
        #2. by default everything is a string, but allow entries in config.json to hard-type columns by name
        schema["properties"][column] = {"type": ["null", "string"]}
    
    return schema
