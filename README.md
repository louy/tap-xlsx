# tap-xlsx

This is a [Singer](https://singer.io) tap that produces JSON-formatted data
following the [Singer
spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md).

This tap:

- Pulls raw data from Excel files
- Outputs the schema for each resource
- Outputs data from the excel file

---

Copyright &copy; 2019 Louay Alakkad
