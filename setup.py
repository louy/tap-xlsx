#!/usr/bin/env python
from setuptools import setup

setup(
    name="tap-xlsx",
    version="0.1.0",
    description="Singer.io tap for extracting data from XLSX files",
    author="Louay Alakkad",
    url="https://louay.alakkad.me",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    py_modules=["tap_xlsx"],
    install_requires=[
        "singer-python>=5.7.0",
        'pandas==0.24.2',
        'xlrd==1.2.0',
    ],
    entry_points="""
    [console_scripts]
    tap-xlsx=tap_xlsx:main
    """,
    packages=["tap_xlsx"],
    package_data = {
    },
    include_package_data=True,
)
